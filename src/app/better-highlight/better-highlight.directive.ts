import { Directive, ElementRef, Renderer2, OnInit, HostListener, HostBinding, Input } from '@angular/core';

@Directive({
  selector: '[appBetterHighlight]'
})
export class BetterHighlightDirective implements OnInit {
  @Input() defaultColor:string="transparent";
  @Input('appBetterHighlight') highlightColor:string="coral";
  @HostBinding('style.backgroundColor') bkColor:string=this.defaultColor;

  constructor(private eleRef:ElementRef,private ren:Renderer2) {    
    
   }

  ngOnInit(): void {
    //this.ren.setStyle(this.eleRef.nativeElement,'background-color', 'coral');
    this.bkColor=this.defaultColor;
  }

  @HostListener('mouseenter') mouseover(eventData:Event){
    //this.ren.setStyle(this.eleRef.nativeElement,'background-color', 'coral');
    this.bkColor=this.highlightColor;

    //console.log(eventData);
  }

  @HostListener('mouseleave') mouseleave(eventData:Event){
    //this.ren.setStyle(this.eleRef.nativeElement,'background-color', 'transparent');
    this.bkColor=this.defaultColor;
    //console.log(eventData);
  }  
}
